<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TableController extends Controller
{
    public function table() {
        return view ('halaman.table');
    }

    public function dataTables() {
        return view ('halaman.data-tables');
    }
}
