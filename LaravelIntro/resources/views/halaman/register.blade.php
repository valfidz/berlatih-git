@extends('layout.master')

@section('title')
    Buat Account Baru!
@endsection

@section('content')
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="post">
        @csrf

        <label for="namaAwal">First name: </label> <br><br>
        <input type="text" name="namaAwal" id="namaAwal">

        <br><br>

        <label for="namaAkhir">Last name: </label> <br><br>
        <input type="text" name="namaAkhir" id="namaAkhir">

        <br><br>

        <label for="gender">Gender: </label> <br><br>
        <input type="radio" value="1" name="gender" id="gender">Male <br>
        <input type="radio" value="2" name="gender" id="gender">Female <br>
        <input type="radio" value="3" name="gender" id="gender">Other <br>

        <br><br>

        <label for="nationality">Nationality: </label><br><br>
        <select name="nationality" id="nationality">
            <option value="1">Indonesian</option>
            <option value="2">Others</option>
        </select>

        <br><br>

        <label for="language">Language Spoken: </label><br><br>
        <input type="checkbox" name="language" id="language">Bahasa Indonesia <br>
        <input type="checkbox" name="language" id="language">English <br>
        <input type="checkbox" name="language" id="language">Other

        <br><br>

        <label for="bio">Bio: </label><br><br>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea>
        <br>
        <button type="submit">Sign Up</button>

    </form>
@endsection
