<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create() {
        return view ('cast.create');
    }

    public function store(Request $request) {
        $request->validate([
            'nama' => 'required|min:2',
            'umur' => 'required',
            'bio' => 'required'
        ],
        [
            'nama.required' => 'Nama harus diisi, tidak boleh kosong',
            'nama.min' => 'Nama minimal 2 karakter',
            'umur.required' => 'Umur harus diisi, tidak boleh kosong',
            'bio.required' => 'Bio harus diisi, tidak boleh kosong'
        ]
    );
        $query = DB::table('cast')->insert([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]);
        return redirect('/cast');
    }

    public function index(){
        $cast = DB::table('cast')->get();
        return view('cast.index', compact('cast'));
    }

    Public function show($id) {
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('cast.show', compact('cast'));
    }

    Public function edit($id) {
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('cast.edit', compact('cast'));
    }

    Public function update($id, Request $request) {
        $request->validate([
            'nama' => 'required|min:2',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        $query = DB::table('cast')
            ->where('id', $id)
            ->update([
                'nama' => $request["nama"],
                'umur' => $request["umur"],
                'bio' => $request["bio"]
            ]);
        return redirect('/cast');
    }

    
public function destroy($id)
{
    $query = DB::table('cast')->where('id', $id)->delete();
    return redirect('/cast');
}
}
