@extends('layout.master')

@section('title')
    Detail Cast
@endsection

@section('content')


<h1 class='text-primary'>Cast List</h1>
<h4>{{$cast->nama}}</h4>
<p>Umur: {{$cast->umur}}</p>
<p>Bio: {{$cast->bio}}</p>

<a href="/cast" class="btn btn-secondary btn-sm">Kembali</a>

@endsection