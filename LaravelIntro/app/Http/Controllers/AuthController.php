<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function regis() {
        return view ('halaman.register');
    }

    public function welcome(Request $request) {
        $namaAwal = request('namaAwal');
        $namaAkhir = request('namaAkhir');

        return view ('halaman.welcome', ['namaAwal' => $namaAwal, 'namaAkhir' => $namaAkhir]);
    }
}
